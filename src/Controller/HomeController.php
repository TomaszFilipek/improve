<?php

namespace App\Controller;

use App\Repository\EmployeeRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(EmployeeRepository $employeeRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'employees' => $employeeRepository->findAll()
        ]);
    }
}
