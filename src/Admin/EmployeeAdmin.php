<?php

namespace App\Admin;

use App\Entity\Employee;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class EmployeeAdmin extends AbstractAdmin
{
    public function toString($object)
    {
        return $object instanceof Employee
            ? $object->getName()
            : 'Employee';
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form->add('name', TextType::class)
            ->add('surname', TextType::class)
            ->add('email', EmailType::class)
            ->add('phoneNumber', NumberType::class)
            ->add('specialization', ModelType::class, [
                'multiple' => true,
                'property' => 'name'
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid->add('name');
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->addIdentifier('name')
            ->addIdentifier('surname')
            ->addIdentifier('email')
            ->addIdentifier('phoneNumber')
            ->add('specialization', 'sonata_type_model', array('multiple' => true, 'by_reference' => false))
            ->addIdentifier('createdAt');
    }
}
